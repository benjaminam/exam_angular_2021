import { PredictionService } from './../prediction.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})

export class StudentsComponent implements OnInit {

  students:Student[];
  students$;
  addStudentFormOpen=false;
  panelOpenState = false;

  add(student:Student){
    this.studentsService.addStudent(student.bagrut, student.psycho, student.paid)
  }
 
  
  deleteStudent(id:string){
    this.studentsService.deleteStudent(id);
  }
  constructor(private studentsService:StudentsService,public authService:AuthService
    ) { }

  ngOnInit(): void {
    this.students$ = this.studentsService.getStudent();
        this.students$.subscribe(
          docs =>{
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              if(student.result){
                student.saved = true; 
              }
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
  }

}
