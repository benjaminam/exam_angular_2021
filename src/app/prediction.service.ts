import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://7jntcj7i6f.execute-api.us-east-1.amazonaws.com/default'; 
  
  predict(bagrut:number, psycho:number,oaid:boolean):Observable<any>{
    let json = {
      "data": 
        {
          "bagrut": bagrut,
          "psycho": psycho,
          "paid": true,

        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

  
  constructor(private http: HttpClient) { }
}
