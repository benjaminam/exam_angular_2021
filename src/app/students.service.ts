import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentsCollection:AngularFirestoreCollection = this.db.collection('students');

  public getStudent():Observable<any[]>{
    this.studentsCollection = this.db.collection(`students`);
    return this.studentsCollection.snapshotChanges()
  }

  updateRsult( id:string,result:string){
    this.db.doc(`students`).update(
      {
        result:result
      })
    }

  public deleteStudent(id:string){
    this.db.doc(`students/${id}`).delete();
  }

addStudent(bagrut:number, psycho:number, paid:boolean){
  const student = {bagrut:bagrut, psycho:psycho, paid:paid}
this.studentsCollection.add(student)} 

  constructor(private db: AngularFirestore)
     { }
}
