import { Student } from './../interfaces/student';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'studentform',
  templateUrl:'./student-form.component.html',
  styleUrls:['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {


  @Input() bagrut: number;
  @Input() psycho: number;
  @Input() id: string;
  @Input() paid: boolean;
  
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();

  onSubmit(){ 

  }  
  
      
  tellParentToClose(){
    this.closeEdit.emit();
  }
  updateParent(){
    let student:Student = {id:this.id,bagrut:this.bagrut, psycho:this.psycho,paid:this.paid}; 
    this.update.emit(student);
      this.bagrut = null;
      this.psycho = null;
      this.paid = null; 
    }
  
  constructor() { }

  ngOnInit(): void {
  }

}
