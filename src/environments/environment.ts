// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyDTiINbZXx6pRn7zKHadQN9VoX95JI0Law",
    authDomain: "exambenjaminemram.firebaseapp.com",
    projectId: "exambenjaminemram",
    storageBucket: "exambenjaminemram.appspot.com",
    messagingSenderId: "786944796204",
    appId: "1:786944796204:web:7dd90622157602419b9389"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
